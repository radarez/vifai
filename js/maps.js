    $(document).ready(function(){
      var url = GMaps.staticMapURL({
        size: [610, 350],
        lat: 18.9167,
        lng: -99.25,
        markers: [
          {lat: 18.9167, lng: -99.25},
        ]
      });
      $('<img/>').attr('src', url).appendTo('#viajes');
    });
