

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {
        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Juan Perez', 3],
          ['Adrian Miranda', 1],
          ['Brenda Rodriguez', 1],
          ['Mauricio Aparicio', 1],
          ['Alejandra Castro', 2]
        ]);

        // Set chart options
        var options = {'title':'Comparativo de viajes por funcionario',
                       'width':500,
                       'height':400};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    
      function viajes_por_año_institucion() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Costo por año'],
          ['2013',  1000,      ],
          ['2014',  1170,      ],
          ['2015',  660,       ],
          ['2016',  1030,      ]
        ]);

        var options = {
          title: 'Viajes por año de IFAI',
          hAxis: {title: 'Year',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('char_viajes_por_año_institucion'));
        chart.draw(data, options);
      }
