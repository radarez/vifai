 <section class="main-section">
    <div class="row">
      <div class="large-12 columns ">
        <br>
        Modulos estadistico
        <div id="chart_div"></div>
        <div id="char_viajes_por_año_institucion"></div>
      </div>
    </div>
  </section>

  <section>
  	<div class="row">
      <div class="large-12 columns ">
        <br>
        Comparte la información del IFAI con tus amigos
        <center><div id="chart_div"></div></center>
        <center><div id="char_viajes_por_año_institucion"></div></center>
      </div>
	    <div class="large-12 columns ">
		  	<div class="fb-comments" data-href="http://www.vifai.radarez.com" data-numposts="5" data-colorscheme="light"></div>
	    </div>
    </div>
  </section>
 

 <!-- Script de Estadistícas -->
 <script language='JavaScript' type='text/javascript' src='<?php echo base_url(); ?>js/estadisticas.js'></script>