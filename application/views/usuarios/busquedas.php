<section class="panel callout radius">
	<div class="row">
		<form>
			<?php
   			// Rules of validation
   			echo validation_errors('<div class="error error_box">','</div>');
				echo "<form name='input' action='buscar_nombre_funcionario' method='get'>";
			?>
		  <div class="small-12 large-9 columns">
		  	<div class="row collapse">
		        <div class="small-8 columns">
		          <input type="text" placeholder="Encuantra a un funcionario" value="Adrian">
		        </div>
		        <div class="small-4 columns">	
		          <a href="#" class="button postfix">Buscar</a>
		        </div>
				</div>
		  </div>
		</form>
	</div>
</section>

<section>
	<div class="row">
	  <div class="small-12 large-12 columns">
	  	<div class="row collapse">
	        <table> 
	        	<table>
  <thead>
    <tr>
      <th>Foto</th>
      <th>Nombre</th>
      <th>Institucion</th>
      <th>Comisiones</th>
      <th>Viajes Nacionales</th>
      <th>Internacionales</th>
      <th>Gastos de Viajes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><img src="<?php echo base_url();?>/img/adrian.jpg"></td>
      <td><a href="<?php echo base_url();?>ver_perfil">Adrián Miranda Aparicio</a></td>
      <td>IFAI</td>
      <td>2</td>
      <td>2</td>
      <td>0</td>
      <td>$10,000.00 MXN</td>
    </tr>
    <tr>
      <td><img src="<?php echo base_url();?>/img/adrian2.jpg"></td>
      <td>Adrian Hernandez Gutierrez</td>
      <td>IFAI</td>
      <td>5</td>
      <td>3</td>
      <td>2</td>
      <td>$20,000.00 MXN</td>
    </tr>
    <tr>
      <td><img src="<?php echo base_url();?>/img/alejandra.jpg"></td>
      <td>Adriana Valladarez Esparza</td>
      <td>IFAI</td>
      <td>7</td>
      <td>5</td>
      <td>2</td>
      <td>$80,000.00 MXN</td>
    </tr>
  </tbody>
</table>
			</div>
	  </div>
	</div>
</section>