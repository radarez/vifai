  <div class="row">
    <div class="small-7 columns">
      <h4>Adrian Miranda A.</h4>
        <table>
          <tbody>
            <tr>
              <td>Origen del Viaje:</td>
              <td>DF</td>
              </tr>
              <td>Destino del Viaje:</td>
              <td>Morelos</td>
              </tr>
              <td>Presupuesto invertido:</td>
              <td>$6,000.00 MXN</td>
              </tr>
              <td>Tema:</td>
              <td>Politicas de información</td>
              </tr>
            </tr>
          </tbody>
        </table>

    </div>
    <div class="small-5 columns">
     
    </div>
  </div>

  <section class="small-12 columns">
    <center><div id="viajes"></div></center>
  </section>

  <div class="row">
    <div class="small-12">
       <h5>Otras Comisiones</h5>      
        <table>
          <thead>
            <tr>
              <th>Tema</th>
              <th>Monto Viaticos</th>
              <th>Origen</th>
              <th>Destino</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>VINCLACION CON ESTADOS Y MUNICIPIOS</td>
              <td>$6,000.00</td>
              <td>DF</td>
              <td>Hidalgo</td>
            </tr>
            <tr>
              <td>VINCLACION CON ESTADOS Y MUNICIPIOS</td>
              <td>$12,000.00</td>
              <td>DF</td>
              <td>Monterrey</td>
            </tr>
            <tr>
              <td>NORMATIVIDAD DE DATOS PERSONALES</td>
              <td>$8,000.00</td>
              <td>DF</td>
              <td>Oaxaca</td>
            </tr>
          </tbody>
        </table>
    </div>
  </div>

   <!-- Script de Estadistícas -->
 <script language='JavaScript' type='text/javascript' src='<?php echo base_url(); ?>js/maps.js'></script>