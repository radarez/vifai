	<section>
	  <footer class="row">
	    <div class="large-12 columns">
	      <hr/>
	      <div class="row">
	        <div class="large-4 columns">
		        <div
					  class="fb-like"
					  data-share="true"
					  data-width="450"
					  data-show-faces="true">
					  </div>
	        </div>
	        <div class="large-4 columns">
						<a href="https://twitter.com/heyaparicio" class="twitter-follow-button" data-show-count="false">Follow @heyaparicio</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>		        
	        </div>
	        <div class="large-4 columns">
	          <ul class="inline-list right">
	            <img src="<?php echo base_url(); ?>img/organizadores.png" alt="">
	          </ul>
	        </div>
	      </div>
	    </div>
	  </footer>
	</section>

  <a class="exit-off-canvas"></a>

  </div>
</div>	
</section>

<script type="text/javascript" src="<?php echo base_url();?>foundation_sass/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>foundation_sass/js/app.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>foundation_sass/bower_components/foundation/js/foundation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>foundation_sass/bower_components/foundation/js/vendor/fastclick.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/gmap/gmap.js"></script>


<script>
  $(document).foundation();
</script>

<script>
// Maps
  $(document).ready(function(){
    var url = GMaps.staticMapURL({
      size: [610, 350],
      lat: 18.9167,
      lng: -99.25,
      markers: [
        {lat: 18.9167, lng: -99.25},
      ]
    });
    $('<img/>').attr('src', url).appendTo('#viajes');
  });
// /Maps

// Gaficas
  google.load('visualization', '1.0', {'packages':['corechart']});
  google.setOnLoadCallback(drawChart);
  google.setOnLoadCallback(viajes_por_año_institucion);
// /Gaficas
  </script>

</body>
</html>

