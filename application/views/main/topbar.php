<section>
<div class="off-canvas-wrap docs-wrap" data-offcanvas="">
  <div class="inner-wrap">
    <nav class="tab-bar">
      <section class="left-small">
        <a aria-expanded="false" class="left-off-canvas-toggle menu-icon"><span></span></a>
      </section>

      <section class="right tab-bar-section">
        <h1 class="title"><img src="<?php echo base_url();?>/img/vifai_logo.png" alt=""></h1>
      </section>

    </nav>

    <?php
    if ($this->session->userdata('level_user')=="ciudadano") 
    {
       include('lmenu_users.php');
     } 
     if ($this->session->userdata('level_user')=="funcionario") 
    {
       include('lmenu_funcionarios.php');
     } 
     if ($this->session->userdata('level_user')=="admin_l") 
    {
       include('lmenu_admin_l.php');
     } 
     if ($this->session->userdata('level_user')=="admin_g") 
    {
       include('lmenu_admin_g.php');
    } 
    ?>
    
