    <aside class="left-off-canvas-menu">
      <ul class="off-canvas-list">
        <li><label>Administrador Global</label></li>
        <li><a href="<?php echo base_url();?>catalogos/admin_g/">Administradores Locales</a></li>
        <li><label>Catálogos</label></li>
        <li><a href="<?php echo base_url();?>catalogos/instituciones/">Instituciones</a></li>
        <li><a href="<?php echo base_url();?>catalogos/paises/">Paises</a></li>
        <li><a href="<?php echo base_url();?>catalogos/estados/">Estados</a></li>
        <li><a href="<?php echo base_url();?>catalogos/municipios/">Municipios</a></li>
        <li><label>Permisos por Institucion</label></li>
        <li>Gestionar Modulos</li>
        <li><label>Configuracion</label></li>
        <li>Variables globales</li>
        <li><label>Sesión</label></li>
        <li>Configurar sesión</li>
        <li><a href="<?php echo base_url();?>seguridad_controller">Cerrar sesión</a></li>
      </ul>
    </aside>