<!DOCTYPE html>
<html lang="es" charset="UTF-8">
<head>		
<meta charset="UTF-8">	
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Vifai | Viajes Transparentes</title>

<link rel="shortcut icon" href="<?php echo base_url();?>img/favicon.ico" /> 
<link type="text/css" rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto+Slab|Roboto|Roboto+Condensed'>
<link type='text/css' rel='stylesheet' href='<?php echo base_url();?>foundation_sass/stylesheets/app.css'  />
<link type='text/css' rel='stylesheet' href='<?php echo base_url();?>css/style.css'  />
<link type='text/css' rel='stylesheet' href='<?php echo base_url();?>css/examples.css'  />
</head>
<body>

<!-- API Facebook  -->
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1469451173334309',
      xfbml      : true,
      version    : 'v2.1'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<!-- API Facebook  -->

<!-- Comments -->
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&appId=1469451173334309&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- Comments -->

<div id="fb-root"></div>

