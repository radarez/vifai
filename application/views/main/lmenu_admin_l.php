    <aside class="left-off-canvas-menu">
      <ul class="off-canvas-list">
        <li><label>Administrador local</label></li>
        <li><label>Catálogos</label></li>

        <li><a href="<?php echo base_url();?>panel_admin_l">Inicio</a></li>
        <li><a href="<?php echo base_url();?>catalogos/admin_l/">Usuarios</a></li>
        <li><a href="<?php echo base_url();?>catalogos/unidades_administrativas">Unidades administrativas</a></li>
        <li><a href="<?php echo base_url();?>catalogos/puestos_superiores">Puestos superiores</a></li>
        <li><a href="<?php echo base_url();?>catalogos/puestos">Puestos</a></li>
        <li><a href="<?php echo base_url();?>catalogos/personal/">Personal</a></li>
        <li><a href="<?php echo base_url();?>catalogos/temas_viajes">Temas</a></li>
        <li><a href="<?php echo base_url();?>catalogos/justificacion_viaticos">Justificación Viaticos</a></li>
        <li><a href="<?php echo base_url();?>catalogos/tipos_comisiones">Tipo de Comision</a></li>
        <li><a href="<?php echo base_url();?>catalogos/tabulador_viaticos">Tabulador</a></li>
        <li><a href="<?php echo base_url();?>catalogos/zonas">Zonas</a></li>

        <li><label>Configuración</label></li>
        <li><a href="<?php echo base_url();?>seguridad_controller">Cerrar sesión</a></li>
      </ul>
    </aside>