<div class="large-8 large-centered  columns	">
      <img class="hide-for-small-only" src="<?php echo base_url();?>/img/vifai_word.png" alt="">
        <section class="section">
          <h5 class="title">Bienvenido a Viajes Transparentes</h5>
          <div class="content panel callout radius panel-login-color" data-slug="panel1">
          <h5 class="title">Administrador global:</h5>
            <?php
            echo validation_errors('<div class="error error_box alert alert-danger">','</div>');
            $formAttributes = array('id' => 'frmLogin', 'name' => 'frmLogin', 'role' =>'form');
            echo form_open("log_administradores_globales", $formAttributes) ;
            echo '<input type="hidden" name="idinstitucion" value="" />';
            ?> 
              <div class="row collapse">
                <div class="large-2 columns">
                  <label class="inline">Correo/Usuario</label>
                </div>
                <div class="large-10 columns">
                  <input type="text" id="yourEmail" placeholder="adrian@institucion.gob">
                </div>
              </div>
              <div class="row collapse">
                <div class="large-2 columns">
                  <label class="inline">Contraseña</label>
                </div>
                <div class="large-10 columns">
                  <input type="password" id="yourName" placeholder="******">
                </div>
              </div>
              <a href="<?php echo base_url(); ?>panel_admin_g" class="button radius">Aceptar</a>
            </form>
          </div>
        </section>

      </div>
    </div>