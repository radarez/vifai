<div class="large-8 large-centered  columns ">
      <img class="hide-for-small-only" src="<?php echo base_url();?>/img/vifai_word.png" alt="">
        <section class="section">
          <h5 class="title">Bienvenido a Viajes Transparentes</h5>
          <div class="content panel callout radius panel-login-color" data-slug="panel1">
          <h5 class="title">Aceso para ciudadanos registrados:</h5>
            <form>
              <div class="row collapse">
                <div class="large-2 columns">
                  <label class="inline">Correo</label>
                </div>
                <div class="large-10 columns">
                  <input type="text" id="yourEmail" placeholder="javier@hotmail.com">
                </div>
              </div>
              <div class="row collapse">
                <div class="large-2 columns">
                  <label class="inline">Contraseña</label>
                </div>
                <div class="large-10 columns">
                  <input type="password" id="yourName" placeholder="******">
                </div>
              </div>
              <a href="<?php echo base_url(); ?>panel_ciudadanos" class="button radius">Aceptar</a>
              <label><a>Recuperar constraseña</a></label>
              <hr>
              <div class="panel callout radius">
                <a class="medium success button radius">Registrate con tu Email</a>
                <hr>
                <h5>Entrar con: </h5>
                <a><img src="<?php echo base_url();?>/img/login_facebook.png"></a>
                <a><img src="<?php echo base_url();?>/img/login_twitter.png"></a>
              </div>
            </form>
          </div>
        </section>

      </div>
    </div>