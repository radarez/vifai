  <nav class="top-bar" data-topbar="" role="navigation">
    <ul class="title-area">
      <li class="name">
        <a href="#"><img class="hide-for-small-only" src="<?php echo base_url();?>/img/logo-ifai.png"></a>
        <a href="#"><img class="hide-for-large-up show-for-small-only" src="<?php echo base_url();?>/img/vifai_logo.png"></a>
      </li>
      <li class="toggle-topbar menu-icon">
      	<a href="#"><span>Menu</span></a>
      </li>
    </ul>

    <section class="top-bar-section">
	    <!-- Right Nav Section -->
	    <ul class="right">
	      <li class="active"><a href="<?php echo base_url();?>log_ciudadanos">Ciudadanos</a></li>
        <li class="active"><a href="<?php echo base_url();?>log_funcionarios">Funcionarios</a></li>
        <li class="active"><a href="<?php echo base_url();?>log_administradores_locales">Administradores Locales</a></li>
	      <li class="active"><a href="<?php echo base_url();?>log_administradores_globales">Administradores Globales</a></li>
	    </ul>
    </section>
  </nav>
