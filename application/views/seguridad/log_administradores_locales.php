<div class="large-8 large-centered  columns	">
      <img class="hide-for-small-only" src="<?php echo base_url();?>/img/vifai_word.png" alt="">
        <section class="section">
          <h5 class="title">Bienvenido a Viajes Transparentes</h5>
          <div class="content panel callout radius panel-login-color" data-slug="panel1">
          <h5 class="title">Administrador local</h5>
            <?php
            echo validation_errors('<div class="error error_box ">','</div>');
            $formAttributes = array('id' => 'frmLogin', 'name' => 'frmLogin');
            echo form_open("panel_admin_l", $formAttributes) ;
            ?> 
              <div class="row collapse">
                <div class="large-2 columns">
                  <label class="inline">Institución</label>
                </div>
                <div class="large-10 columns">
                  <label>
						        <select>
                      <option name="idinstitucion" id="idinstitucion" value="">Seleccione una institución</option>
                      <?php
                        foreach ($instituciones as $datos) 
                        {
                          echo "<option value=".$datos->idinstitucion.">".$datos->siglas."-".$datos->institucion."</option>";
                        }
                      ?>
                    </select>
						      </label>
                </div>
              </div>
              <div class="row collapse">
                <div class="large-2 columns">
                  <label class="inline">Correo/Usuario</label>
                </div>
                <div class="large-10 columns">
                  <input type="text" id="yourEmail" placeholder="javier@hotmail.com">
                </div>
              </div>
              <div class="row collapse">
                <div class="large-2 columns">
                  <label class="inline">Contraseña</label>
                </div>
                <div class="large-10 columns">
                  <input type="password" id="yourName" placeholder="******">
                </div>
              </div>
              <input class="button radius" type="submit" name="mysubmit" value="Aceptar" />
              <label><a>Contactar al administrador</a></label>
              <hr>
            </form>
          </div>
        </section>

      </div>
    </div>
