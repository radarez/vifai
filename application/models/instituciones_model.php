<?php
class Instituciones_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
 
     //Search profile user
    function listar_instituciones($estatus)
    {
        $where=array("estatus"=>$estatus);
        $query=$this->db
        ->select("idinstitucion, institucion, siglas")
        ->from("vifai.instituciones")
        ->where($where)
        ->get();
        //Return result
        // echo $this->db->last_query();
        return $query->result();
    }
 
}