<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_Controller extends CI_Controller {

	public function index()
	{
		$this->load->view('main/hIndex');
		$this->load->view('seguridad/login');
	}

	public function panel_ciudadanos()
	{
		$settings["level"]="ciudadanos";
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar', $settings);
		$this->load->view('main/cIndex');
		$this->load->view('main/fIndex');
	}

	public function panel_funcionarios()
	{
		$settings["level"]="funcionarios";
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar', $settings);
		$this->load->view('main/cIndex');
		$this->load->view('main/fIndex');
		
	}

	public function panel_admin_l()
	{
		echo $this->input->post('idinstitucion')."-kkk";
		$this->session->set_userdata('idinstitucion', $this->input->post('idinstitucion'));
		$settings["level"]="admin_l";
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar', $settings);
		$this->load->view('main/cIndex');
		$this->load->view('main/fIndex');
	}

	public function panel_admin_g()
	{
		$settings["level"]="admin_g";
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar', $settings);
		$this->load->view('main/cIndex');
		$this->load->view('main/fIndex');
	}
	

	public function variables_session()
		
	{
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('main/cIndex');
		$this->load->view('main/fIndex');
	}

}

