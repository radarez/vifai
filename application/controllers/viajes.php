<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Viajes extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}

	public function _example_output($output = null)
	{
		$this->load->view('example.php',$output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	/*Reporte de Viajes*/
	public function reporte_viajes($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("reporte_viajes");
		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('example.php',$output);
		$this->load->view('main/fIndex');
	}
}