<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catalogos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}

	public function _example_output($output = null)
	{
		$this->load->view('catalogos/catalogos.php',$output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	/*Catalogo de dependencias*/
	public function instituciones($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("instituciones");
		$crud->field_type('estatus','dropdown',
        	array( "activo"  => "Activo", "inactivo" => "Inactivo"));
		// $crud->unset_edit();
		// $crud->add_action('', '', '','ui-icon-pencil');
		$crud->unset_jquery();
		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('catalogos/catalogos.php',$output);
		$this->load->view('main/fIndex');
	}

	/*Catalogo de tipo de comisiones*/
	public function tipos_comisiones($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("tipos_comisiones");
		$crud->field_type('estatus','dropdown',
        	array( "activo"  => "Activo", "inactivo" => "Inactivo"));
		$crud->set_relation('idinstitucion','instituciones','institucion');
		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('catalogos/catalogos.php',$output);
		$this->load->view('main/fIndex');
	}

	/*Catalogo de unidades_administrativas*/
	public function unidades_administrativas($output = null)
	{
		echo $this->session->userdata('idinstitucion');
		echo "sss";
		$crud=new grocery_CRUD();
		$crud->set_table("unidades_administrativas");
		$crud->field_type('estatus','dropdown',
        	array( "activo"  => "Activo", "inactivo" => "Inactivo"));
		$crud->set_relation('idinstitucion','instituciones','institucion');
		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('catalogos/catalogos.php',$output);
		$this->load->view('main/fIndex');
	}

	/*Catalogo de puestos_superiores*/
	public function puestos_superiores($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("puestos_superiores");
		$crud->field_type('estatus','dropdown',
        	array( "activo"  => "Activo", "inactivo" => "Inactivo"));
		$crud->set_relation('idunidad_administrativa','unidades_administrativas','unidad_administrativa');
		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('catalogos/catalogos.php',$output);
		$this->load->view('main/fIndex');
	}

	/*Catalogo de puestos*/
	public function puestos($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("puestos");
		$crud->field_type('estatus','dropdown',
        	array( "activo"  => "Activo", "inactivo" => "Inactivo"));
		$crud->set_relation('idtabulador_viaticos','tabulador_viaticos','grupo_jerarquico');
		$crud->set_relation('idpuesto_superior','puestos_superiores','cargo_superior');
		$crud->display_as('idtabulador_viaticos','Grupo jerarquico');
		$crud->display_as('idpuesto_superior','Puesto superior');
		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('catalogos/catalogos.php',$output);
		$this->load->view('main/fIndex');
	}

	/*Catalogo de tabulador_viaticos*/
	public function tabulador_viaticos($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("tabulador_viaticos");
		$crud->field_type('estatus','dropdown',
        	array( "activo"  => "Activo", "inactivo" => "Inactivo"));
		$crud->set_relation('clave_zona','zonas','clave_zona');
		$crud->field_type('tipo_viaje','dropdown',
        	array( "nacional"  => "Nacional", "internacional" => "Internacional"));
		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('catalogos/catalogos.php',$output);
		$this->load->view('main/fIndex');
	}
	/*Catalogo de zonas*/
	public function zonas($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("zonas");
		$crud->set_relation('idpais','paises','pais');
		$crud->set_relation('idestado','estados','estado');
		$crud->set_relation('idmunicipio','municipios','municipio');
		$crud->field_type('tipo_viaje','dropdown',
        	array( "nacional"  => "Nacional", "internacional" => "Internacional"));
		$crud->display_as('idpais','País');
		$crud->display_as('idmunicipio','Municipios');
		$crud->display_as('idestado','Estado');

		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('catalogos/catalogos.php',$output);
		$this->load->view('main/fIndex');
	}
	
	/*Catalogo de personal*/
	public function personal($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("personal");
		$crud->field_type('estatus','dropdown',
        	array( "activo"  => "Activo", "inactivo" => "Inactivo"));
		$crud->set_relation('idpuesto','puestos','nombre_puesto');
		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('catalogos/catalogos.php',$output);
		$this->load->view('main/fIndex');
	}

	/*Catalogo de temas viajes */
	public function temas_viajes($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("temas_viajes");
		$crud->field_type('estatus','dropdown',
        	array( "activo"  => "Activo", "inactivo" => "Inactivo"));
		$crud->set_relation('idinstitucion','instituciones','institucion');
		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('catalogos/catalogos.php',$output);
		$this->load->view('main/fIndex');
	}

	/*Catalogo de justificacion viaticos */
	public function justificacion_viaticos($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("justificacion_viaticos");
		$crud->field_type('estatus','dropdown',
        	array( "activo"  => "Activo", "inactivo" => "Inactivo"));
		$crud->display_as('justificacion','Justificación');
		 $crud->field_type('justificacion', 'text');
		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('catalogos/catalogos.php',$output);
		$this->load->view('main/fIndex');
	}

	/*Formulario de captura de Viaticos*/
	public function viaticos_comprobantes($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("viaticos_comprobantes");
		$crud->field_type('estatus','dropdown',
        	array( "activo"  => "Activo", "inactivo" => "Inactivo"));
		$crud->display_as('idcomision','Clave comisión');
		 $crud->field_type('justificacion', 'text');
		$crud->set_relation('idcomision','comisiones','idcomision');
		$crud->set_relation('idjustificacion_viatico','justificacion_viaticos','justificacion');
		$crud->display_as('idjustificacion_viatico','Justificación');
		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('catalogos/catalogos.php',$output);
		$this->load->view('main/fIndex');
	}

	/*Paises*/
	public function paises($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("paises");
		$crud->field_type('estatus','dropdown',
        	array( "activo"  => "Activo", "inactivo" => "Inactivo"));
		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('catalogos/catalogos.php',$output);
		$this->load->view('main/fIndex');
	}

	/*Estados*/
	public function estados($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("estados");
		$crud->field_type('estatus','dropdown',
        	array( "activo"  => "Activo", "inactivo" => "Inactivo"));
		$crud->set_relation('idpais','paises','pais');
		$crud->field_type('estatus','dropdown',
        	array( "activo"  => "Activo", "inactivo" => "Inactivo"));
		$crud->display_as('idpais','País');

		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('example.php',$output);
		$this->load->view('main/fIndex');
	}

	/*Municipios*/
	public function municipios($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("municipios");
		$crud->set_relation('idestado','estados','estado');
		$crud->field_type('estatus','dropdown',
        	array( "activo"  => "Activo", "inactivo" => "Inactivo"));
		$crud->display_as('idestado','Estado');

		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('catalogos/catalogos.php',$output);
		$this->load->view('main/fIndex');
	}

	/*Reporte de Viajes = comisiones*/
	public function comisiones($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("comisiones");
		
		$crud->field_type('tipo_viaje','dropdown',
        	array( "nacional"  => "Nacional", "internacional" => "Internacional"));
		$crud->field_type('mec_origen','dropdown',
        	array( "invitacion"  => "Invitación", "requerimientoUR" => "Requerimiento UR"));
		$crud->field_type('inst_genera', 'text');
		$crud->field_type('tipo_rep','dropdown',
        	array( "tecnico"  => "Técnico", "alto_nivel" => "Alto nivel"));
		$crud->set_relation('idpersonal','personal','nombre');
		$crud->set_relation('idpersonal','personal','nombre');
		$crud->display_as('idpersonal','Nombre');
		$crud->set_relation('idtabulador_viaticos','tabulador_viaticos','clave_zona');
		$crud->set_relation('idtema_viaje','temas_viajes','tema');
		$crud->display_as('idtema_viaje','Tema');
		$crud->set_relation('idtipo_comision','tipos_comisiones','tipo_comision');
		$crud->display_as('idtipo_comision','Tipo de comisión');
		$crud->field_type('evento', 'text');
		$crud->field_type('url_evento', 'text');
		$crud->field_type('motivo', 'text');
		$crud->field_type('antecedentes', 'text');
		$crud->field_type('resultado', 'text');
		$crud->field_type('contribucion_ifai', 'text');
		$crud->field_type('url_cominicado', 'text');
		$crud->field_type('tipo_pasaje','dropdown',
        	array( "terrestre"  => "Terrestre", "aereo" => "Aéreo", "maritimo" => "Marítimo"));
		$crud->field_type('linea_origen', 'text');
		$crud->field_type('vuelo_origen', 'text');
		$crud->field_type('linea_regreso', 'text');
		$crud->field_type('vuelo_regreso', 'text');
		$crud->display_as('fechainicio_com','Fecha inicio comunicado');
		$crud->display_as('fechafin_com','Fecha fin comunicado');


		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('catalogos/catalogos.php',$output);
		$this->load->view('main/fIndex');
	}

	/*Perfil de funcionarios: se muestra para los usuarios que deciden
	conocer con más detalle sus actividades*/
	public function perfiles_funcionarios($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("perfiles_funcionarios");
		$crud->field_type('estatus','dropdown',
        	array( "activo"  => "Activo", "inactivo" => "Inactivo"));

		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('example.php',$output);
		$this->load->view('main/fIndex');
	}

	/*Catalogo administradores locales*/
	public function admin_l($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("usuarios");
		$crud->field_type('nivel','dropdown',
        	array( "funcionario" => "Funcionarios"));
		$crud->field_type('estatus','dropdown',
        	array( "activo"  => "Activo", "inactivo" => "Inactivo"));
		$crud->set_relation('idinstitucion','instituciones','institucion');
		$crud->display_as('idinstitucion','Institucion de pertenencia');

		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('catalogos/catalogos.php',$output);
		$this->load->view('main/fIndex');
	}

	/*Catalogo administradores globales*/
	public function admin_g($output = null)
	{
		$crud=new grocery_CRUD();
		$crud->set_table("usuarios");
		$crud->field_type('nivel','dropdown',
        	array( "admin_global"  => "Administrador Global", "admin_local" => "Administrador Local", "funcionario" => "Funcionarios"));
		$crud->field_type('estatus','dropdown',
        	array( "activo"  => "Activo", "inactivo" => "Inactivo"));
		$crud->set_relation('idinstitucion','instituciones','institucion');
		$output=$crud->render();
		
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar');
		$this->load->view('catalogos/catalogos.php',$output);
		$this->load->view('main/fIndex');
	}

}
