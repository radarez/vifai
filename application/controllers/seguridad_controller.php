<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seguridad_Controller extends CI_Controller {

	public function index()
	{
		$this->session->set_userdata('level_user', "ciudadano");
		$this->load->view('seguridad/topbar');
		$this->load->view('main/hIndex');
		$this->load->view('seguridad/log_ciudadanos');
		$this->load->view('main/fIndex');
	}

	public function log_ciudadanos()
	{
		$this->session->set_userdata('level_user', "ciudadano");
		$this->load->view('seguridad/topbar');
		$this->load->view('main/hIndex');
		$this->load->view('seguridad/log_ciudadanos');
		$this->load->view('main/fIndex');
	}
	public function log_funcionarios()
	{
		$this->session->set_userdata('level_user', "funcionario");
		$data['instituciones']=$this->Instituciones_Model->listar_instituciones("activo");
		$this->load->view('seguridad/topbar');			
		$this->load->view('main/hIndex');
		$this->load->view('seguridad/log_funcionarios', $data);
		$this->load->view('main/fIndex');
	}
	public function log_administradores_locales()
	{
		$this->session->set_userdata('level_user', "admin_l");
		$data['instituciones']=$this->Instituciones_Model->listar_instituciones("activo");
		$this->load->view('seguridad/topbar');
		$this->load->view('main/hIndex');
		$this->load->view('seguridad/log_administradores_locales', $data);
		$this->load->view('main/fIndex');
		
	}
	public function log_administradores_globales()
	{
		$this->session->set_userdata('level_user', "admin_g");
		$this->load->view('seguridad/topbar');
		$this->load->view('main/hIndex');
		$this->load->view('seguridad/log_administradores_globales');
		$this->load->view('main/fIndex');
		
	}
}


