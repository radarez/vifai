<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	public function estadisticas()
	{
		$settings["level"]="usuarios";
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar', $settings);
		$this->load->view('usuarios/estadisticas');
		$this->load->view('main/fIndex');
	}

	public function busquedas()
	{
		$settings["level"]="usuarios";
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar', $settings);
		$this->load->view('usuarios/busquedas');
		$this->load->view('main/fIndex');
	}

	public function buscar_nombre_funcionario()
	{
		$this->input->post('txtBuscar');
		$settings["level"]="usuarios";
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar', $settings);
		$this->load->view('usuarios/busquedas');
		$this->load->view('main/fIndex');
	}

	public function perfil_usuarios()
	{
		$this->input->post('txtBuscar');
		$settings["level"]="usuarios";
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar', $settings);
		$this->load->view('usuarios/perfil_usuarios');
		$this->load->view('main/fIndex');
	}

	public function perfil_funcionarios()
	{
		$this->input->post('txtBuscar');
		$settings["level"]="usuarios";
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar', $settings);
		$this->load->view('funcionarios/perfil_funcionarios');
		$this->load->view('main/fIndex');
	}
	
	public function ver_perfil()
	{
		$this->input->post('txtBuscar');
		$settings["level"]="usuarios";
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar', $settings);
		$this->load->view('funcionarios/ver_perfil');
		$this->load->view('main/fIndex');
	}
	public function comisiones()
	{
		$this->input->post('txtBuscar');
		$settings["level"]="usuarios";
		$this->load->view('main/hIndex');
		$this->load->view('main/topbar', $settings);
		$this->load->view('usuarios/comisiones');
		$this->load->view('main/fIndex');
	}
	
}
