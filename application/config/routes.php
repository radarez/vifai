<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$route['default_controller'] = "seguridad_controller";
$route['404_override'] = '';

/*Catalogos*/
$route['cat_instituciones'] = "catalogos/instituciones";

/*Paneles*/
$route['panel_ciudadanos'] = "main_controller/panel_ciudadanos";
$route['panel_funcionarios'] = "main_controller/panel_funcionarios";
$route['panel_admin_l'] = "main_controller/panel_admin_l";
$route['panel_admin_g'] = "main_controller/panel_admin_g";

/*Logins*/
$route['log_ciudadanos'] = "seguridad_controller/log_ciudadanos";
$route['log_funcionarios'] = "seguridad_controller/log_funcionarios";
$route['log_administradores_locales'] = "seguridad_controller/log_administradores_locales";
$route['log_administradores_globales'] = "seguridad_controller/log_administradores_globales";

/*Administracion*/
$route['usuarios'] = "examples/usuarios";

/*Estadisticas*/
$route['estadisticas'] = "usuarios/estadisticas";
$route['busquedas'] = "usuarios/busquedas";
$route['perfil_funcionarios'] = "usuarios/perfil_funcionarios";
$route['perfil_usuarios'] = "usuarios/perfil_usuarios";
$route['ver_perfil'] = "usuarios/ver_perfil";
$route['buscar_nombre_funcionario/(:any)'] = "usuarios/buscar_nombre_funcionario/$1";
$route['comisiones'] = "usuarios/comisiones";

/*Reporte de Viajes*/
// $route['reporte_viajes'] = "examples/reporte_viajes";


